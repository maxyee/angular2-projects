System.register(['angular2/core', "./contacts/contact-list.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, contact_list_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (contact_list_component_1_1) {
                contact_list_component_1 = contact_list_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n        <contact-list></contact-list>\n\n        <!--\n        <h3 (click)=\"onSelect()\"\n        [class.clicked]=\"showDetail === true\"\n        >{{ contact.firstname }} {{ contact.lastname }}\n        </h3>\n        -->\n\n        <!--\n        <input [(ngModel)]=\"SelectedContact.firstname\" type=\"text\"/>\n\n        <div>\n            phone Number:{{ SelectedContact.phone }}<br>\n            Email :{{SelectedContact.email }}\n        </div>\n        -->\n    ",
                        directives: [contact_list_component_1.ContactListComponent],
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUEyQkE7Z0JBQUE7Z0JBRUEsQ0FBQztnQkExQkQ7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsUUFBUTt3QkFDbEIsUUFBUSxFQUFFLDBkQWtCVDt3QkFDRCxVQUFVLEVBQUUsQ0FBQyw2Q0FBb0IsQ0FBQztxQkFFckMsQ0FBQzs7Z0NBQUE7Z0JBR0YsbUJBQUM7WUFBRCxDQUZBLEFBRUMsSUFBQTtZQUZELHVDQUVDLENBQUEiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICdhbmd1bGFyMi9jb3JlJztcbmltcG9ydCB7Q29udGFjdExpc3RDb21wb25lbnR9ICBmcm9tIFwiLi9jb250YWN0cy9jb250YWN0LWxpc3QuY29tcG9uZW50XCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnbXktYXBwJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8Y29udGFjdC1saXN0PjwvY29udGFjdC1saXN0PlxuXG4gICAgICAgIDwhLS1cbiAgICAgICAgPGgzIChjbGljayk9XCJvblNlbGVjdCgpXCJcbiAgICAgICAgW2NsYXNzLmNsaWNrZWRdPVwic2hvd0RldGFpbCA9PT0gdHJ1ZVwiXG4gICAgICAgID57eyBjb250YWN0LmZpcnN0bmFtZSB9fSB7eyBjb250YWN0Lmxhc3RuYW1lIH19XG4gICAgICAgIDwvaDM+XG4gICAgICAgIC0tPlxuXG4gICAgICAgIDwhLS1cbiAgICAgICAgPGlucHV0IFsobmdNb2RlbCldPVwiU2VsZWN0ZWRDb250YWN0LmZpcnN0bmFtZVwiIHR5cGU9XCJ0ZXh0XCIvPlxuXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgICBwaG9uZSBOdW1iZXI6e3sgU2VsZWN0ZWRDb250YWN0LnBob25lIH19PGJyPlxuICAgICAgICAgICAgRW1haWwgOnt7U2VsZWN0ZWRDb250YWN0LmVtYWlsIH19XG4gICAgICAgIDwvZGl2PlxuICAgICAgICAtLT5cbiAgICBgLFxuICAgIGRpcmVjdGl2ZXM6IFtDb250YWN0TGlzdENvbXBvbmVudF0sXG4gICAgXG59KVxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XG5cbn1cbiJdfQ==
