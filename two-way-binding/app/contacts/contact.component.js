System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var ContactComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            ContactComponent = (function () {
                function ContactComponent() {
                    this.contact = {};
                }
                ContactComponent = __decorate([
                    core_1.Component({
                        selector: 'contact',
                        template: "\n    <input [(ngModel)]=\"contact.firstname\" type=\"text\"/>\n\n    <div>\n        <div>\n            <label for=\"firstname\"> First Name: </label>\n            <input [(ngModel)]=\"contact.firstname\" type=\"text\" id=\"firstname\"/>\n        </div>\n        <div>\n            <label for=\"lastname\"> Last Name: </label>\n            <input [(ngModel)]=\"contact.lastname\" type=\"text\" id=\"lastname\"/>\n        </div>\n        <div>\n            <label for=\"phone\"> Phone Number: </label>\n            <input [(ngModel)]=\"contact.phone\" type=\"text\" id=\"phone\"/>\n        </div>\n        <div>\n            <label for=\"email\"> Email: </label>\n            <input [(ngModel)]=\"contact.email\" type=\"text\" id=\"email\"/>\n        </div>\n    </div>\n    ",
                        inputs: ["contact"],
                        styles: ["\n        label{\n          display:inline-block;\n          widht:140px;\n        }\n\n        input {\n          width:250px;\n        }\n    "]
                    }), 
                    __metadata('design:paramtypes', [])
                ], ContactComponent);
                return ContactComponent;
            }());
            exports_1("ContactComponent", ContactComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3RzL2NvbnRhY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBdUNBO2dCQUFBO29CQUNXLFlBQU8sR0FBRyxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7Z0JBdkNEO29CQUFDLGdCQUFTLENBQUM7d0JBQ1AsUUFBUSxFQUFFLFNBQVM7d0JBQ25CLFFBQVEsRUFBQyx3d0JBcUJSO3dCQUNELE1BQU0sRUFBRSxDQUFDLFNBQVMsQ0FBQzt3QkFDbkIsTUFBTSxFQUFFLENBQUMsa0pBU1IsQ0FBQztxQkFDTCxDQUFDOztvQ0FBQTtnQkFJRix1QkFBQztZQUFELENBRkEsQUFFQyxJQUFBO1lBRkQsK0NBRUMsQ0FBQSIsImZpbGUiOiJjb250YWN0cy9jb250YWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tIFwiYW5ndWxhcjIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvbnRhY3QnLFxuICAgIHRlbXBsYXRlOmBcbiAgICA8aW5wdXQgWyhuZ01vZGVsKV09XCJjb250YWN0LmZpcnN0bmFtZVwiIHR5cGU9XCJ0ZXh0XCIvPlxuXG4gICAgPGRpdj5cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJmaXJzdG5hbWVcIj4gRmlyc3QgTmFtZTogPC9sYWJlbD5cbiAgICAgICAgICAgIDxpbnB1dCBbKG5nTW9kZWwpXT1cImNvbnRhY3QuZmlyc3RuYW1lXCIgdHlwZT1cInRleHRcIiBpZD1cImZpcnN0bmFtZVwiLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8bGFiZWwgZm9yPVwibGFzdG5hbWVcIj4gTGFzdCBOYW1lOiA8L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0IFsobmdNb2RlbCldPVwiY29udGFjdC5sYXN0bmFtZVwiIHR5cGU9XCJ0ZXh0XCIgaWQ9XCJsYXN0bmFtZVwiLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8bGFiZWwgZm9yPVwicGhvbmVcIj4gUGhvbmUgTnVtYmVyOiA8L2xhYmVsPlxuICAgICAgICAgICAgPGlucHV0IFsobmdNb2RlbCldPVwiY29udGFjdC5waG9uZVwiIHR5cGU9XCJ0ZXh0XCIgaWQ9XCJwaG9uZVwiLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8bGFiZWwgZm9yPVwiZW1haWxcIj4gRW1haWw6IDwvbGFiZWw+XG4gICAgICAgICAgICA8aW5wdXQgWyhuZ01vZGVsKV09XCJjb250YWN0LmVtYWlsXCIgdHlwZT1cInRleHRcIiBpZD1cImVtYWlsXCIvPlxuICAgICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgICBgLFxuICAgIGlucHV0czogW1wiY29udGFjdFwiXSxcbiAgICBzdHlsZXM6IFtgXG4gICAgICAgIGxhYmVse1xuICAgICAgICAgIGRpc3BsYXk6aW5saW5lLWJsb2NrO1xuICAgICAgICAgIHdpZGh0OjE0MHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgaW5wdXQge1xuICAgICAgICAgIHdpZHRoOjI1MHB4O1xuICAgICAgICB9XG4gICAgYF1cbn0pXG5cbmV4cG9ydCBjbGFzcyBDb250YWN0Q29tcG9uZW50IHtcbiAgICBwdWJsaWMgY29udGFjdCA9IHt9O1xufVxuIl19
