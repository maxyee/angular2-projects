System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var CONTACTS;
    return {
        setters:[],
        execute: function() {
            exports_1("CONTACTS", CONTACTS = [
                { firstname: "Max", lastname: "Maxyee", phone: "01680057586",
                    email: "Maxyee@gmail.com" },
                { firstname: "Eyamin", lastname: "Khan", phone: "01675556582",
                    email: "eyamin@gmail.com" },
                { firstname: "Mohon", lastname: "Khan", phone: "01521213203",
                    email: "mohon@gmail.com" },
                { firstname: "Julhas", lastname: "hossain", phone: "01780015384",
                    email: "julhas@gmail.com" },
            ]);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3RzL21vY2stY29udGFjdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7UUFDYSxRQUFROzs7O1lBQVIsc0JBQUEsUUFBUSxHQUFjO2dCQUNqQyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFHLFFBQVEsRUFBRSxLQUFLLEVBQUUsYUFBYTtvQkFDNUQsS0FBSyxFQUFFLGtCQUFrQixFQUFDO2dCQUMxQixFQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFHLE1BQU0sRUFBRSxLQUFLLEVBQUUsYUFBYTtvQkFDN0QsS0FBSyxFQUFFLGtCQUFrQixFQUFDO2dCQUMxQixFQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFHLE1BQU0sRUFBRSxLQUFLLEVBQUUsYUFBYTtvQkFDNUQsS0FBSyxFQUFFLGlCQUFpQixFQUFDO2dCQUN6QixFQUFDLFNBQVMsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFHLFNBQVMsRUFBRSxLQUFLLEVBQUUsYUFBYTtvQkFDaEUsS0FBSyxFQUFFLGtCQUFrQixFQUFDO2FBQzNCLENBQUEsQ0FBQyIsImZpbGUiOiJjb250YWN0cy9tb2NrLWNvbnRhY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbnRhY3R9IGZyb20gXCIuL2NvbnRhY3RcIjtcbmV4cG9ydCBjb25zdCBDT05UQUNUUzogQ29udGFjdFtdID0gW1xuICB7Zmlyc3RuYW1lOiBcIk1heFwiLCBsYXN0bmFtZSA6IFwiTWF4eWVlXCIsIHBob25lOiBcIjAxNjgwMDU3NTg2XCIgLFxuICBlbWFpbDogXCJNYXh5ZWVAZ21haWwuY29tXCJ9LFxuICB7Zmlyc3RuYW1lOiBcIkV5YW1pblwiLCBsYXN0bmFtZSA6IFwiS2hhblwiLCBwaG9uZTogXCIwMTY3NTU1NjU4MlwiICxcbiAgZW1haWw6IFwiZXlhbWluQGdtYWlsLmNvbVwifSxcbiAge2ZpcnN0bmFtZTogXCJNb2hvblwiLCBsYXN0bmFtZSA6IFwiS2hhblwiLCBwaG9uZTogXCIwMTUyMTIxMzIwM1wiICxcbiAgZW1haWw6IFwibW9ob25AZ21haWwuY29tXCJ9LFxuICB7Zmlyc3RuYW1lOiBcIkp1bGhhc1wiLCBsYXN0bmFtZSA6IFwiaG9zc2FpblwiLCBwaG9uZTogXCIwMTc4MDAxNTM4NFwiICxcbiAgZW1haWw6IFwianVsaGFzQGdtYWlsLmNvbVwifSxcbl07XG4iXX0=
