System.register(["angular2/core", "./contact.component", "./contact.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, contact_component_1, contact_service_1;
    var ContactListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (contact_component_1_1) {
                contact_component_1 = contact_component_1_1;
            },
            function (contact_service_1_1) {
                contact_service_1 = contact_service_1_1;
            }],
        execute: function() {
            ContactListComponent = (function () {
                function ContactListComponent(_contactService) {
                    this._contactService = _contactService;
                    this.contacts = Contact[];
                    this.SelectedContact = {};
                }
                ContactListComponent.prototype.onSelect = function (contact) {
                    this.SelectedContact = contact;
                };
                ContactListComponent.prototype.getContacts = function () {
                    var _this = this;
                    this._contactService.getContacts().then(function (contacts) { return _this.contacts = contacts; });
                };
                ContactListComponent.prototype.ngOnInit = function () {
                    this.getContacts();
                };
                ContactListComponent = __decorate([
                    core_1.Component({
                        selector: 'contact-list',
                        template: "\n    <ul>\n        <li *ngFor=\"#contact of contacts\"\n        (click)=\"onSelect(contact)\"\n        [class.clicked]=\"SelectedContact === contact\"\n        >\n        {{ contact.firstname }} {{ contact.lastname }}\n        </li>\n    </ul>\n    <contact [contact]=\"SelectedContact\"></contact>\n    ",
                        directives: [contact_component_1.ContactComponent],
                        providers: [contact_service_1.ContactService],
                        styleUrls: ["../src/css/contact-list.css"]
                    }), 
                    __metadata('design:paramtypes', [contact_service_1.ContactService])
                ], ContactListComponent);
                return ContactListComponent;
            }());
            exports_1("ContactListComponent", ContactListComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRhY3RzL2NvbnRhY3QtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUF3QkE7Z0JBS0UsOEJBQW9CLGVBQStCO29CQUEvQixvQkFBZSxHQUFmLGVBQWUsQ0FBZ0I7b0JBSjVDLGFBQVEsR0FBRyxPQUFPLEVBQUUsQ0FBQztvQkFFckIsb0JBQWUsR0FBRyxFQUFFLENBQUM7Z0JBRTBCLENBQUM7Z0JBRXZELHVDQUFRLEdBQVIsVUFBUyxPQUFPO29CQUNkLElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDO2dCQUNqQyxDQUFDO2dCQUVELDBDQUFXLEdBQVg7b0JBQUEsaUJBRUM7b0JBREMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFtQixJQUFLLE9BQUEsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLEVBQXhCLENBQXdCLENBQUMsQ0FBQztnQkFDN0YsQ0FBQztnQkFFRCx1Q0FBUSxHQUFSO29CQUNFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDckIsQ0FBQztnQkFuQ0g7b0JBQUMsZ0JBQVMsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsY0FBYzt3QkFDeEIsUUFBUSxFQUFDLG1UQVVSO3dCQUNELFVBQVUsRUFBRSxDQUFDLG9DQUFnQixDQUFDO3dCQUM5QixTQUFTLEVBQUUsQ0FBQyxnQ0FBYyxDQUFDO3dCQUMzQixTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztxQkFDN0MsQ0FBQzs7d0NBQUE7Z0JBb0JGLDJCQUFDO1lBQUQsQ0FsQkEsQUFrQkMsSUFBQTtZQWxCRCx1REFrQkMsQ0FBQSIsImZpbGUiOiJjb250YWN0cy9jb250YWN0LWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gXCJhbmd1bGFyMi9jb3JlXCI7XG5pbXBvcnQge0NvbnRhY3RDb21wb25lbnR9ICBmcm9tIFwiLi9jb250YWN0LmNvbXBvbmVudFwiO1xuaW1wb3J0IHtDb250YWN0U2VydmljZX0gZnJvbSBcIi4vY29udGFjdC5zZXJ2aWNlXCI7XG5pbXBvcnQge0NvbnRhY3R9IGZyb20gXCIuL2NvbnRhY3RcIjtcbmltcG9ydCB7T25Jbml0fSBmcm9tIFwiYW5ndWxhcjIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2NvbnRhY3QtbGlzdCcsXG4gICAgdGVtcGxhdGU6YFxuICAgIDx1bD5cbiAgICAgICAgPGxpICpuZ0Zvcj1cIiNjb250YWN0IG9mIGNvbnRhY3RzXCJcbiAgICAgICAgKGNsaWNrKT1cIm9uU2VsZWN0KGNvbnRhY3QpXCJcbiAgICAgICAgW2NsYXNzLmNsaWNrZWRdPVwiU2VsZWN0ZWRDb250YWN0ID09PSBjb250YWN0XCJcbiAgICAgICAgPlxuICAgICAgICB7eyBjb250YWN0LmZpcnN0bmFtZSB9fSB7eyBjb250YWN0Lmxhc3RuYW1lIH19XG4gICAgICAgIDwvbGk+XG4gICAgPC91bD5cbiAgICA8Y29udGFjdCBbY29udGFjdF09XCJTZWxlY3RlZENvbnRhY3RcIj48L2NvbnRhY3Q+XG4gICAgYCxcbiAgICBkaXJlY3RpdmVzOiBbQ29udGFjdENvbXBvbmVudF0sXG4gICAgcHJvdmlkZXJzOiBbQ29udGFjdFNlcnZpY2VdLFxuICAgIHN0eWxlVXJsczogW1wiLi4vc3JjL2Nzcy9jb250YWN0LWxpc3QuY3NzXCJdXG59KVxuXG5leHBvcnQgY2xhc3MgQ29udGFjdExpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBwdWJsaWMgY29udGFjdHMgPSBDb250YWN0W107XG5cbiAgcHVibGljIFNlbGVjdGVkQ29udGFjdCA9IHt9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX2NvbnRhY3RTZXJ2aWNlOiBDb250YWN0U2VydmljZSkge31cblxuICBvblNlbGVjdChjb250YWN0KXtcbiAgICB0aGlzLlNlbGVjdGVkQ29udGFjdCA9IGNvbnRhY3Q7XG4gIH1cblxuICBnZXRDb250YWN0cygpIHtcbiAgICB0aGlzLl9jb250YWN0U2VydmljZS5nZXRDb250YWN0cygpLnRoZW4oKGNvbnRhY3RzOiBDb250YWN0W10pID0+IHRoaXMuY29udGFjdHMgPSBjb250YWN0cyk7XG4gIH1cblxuICBuZ09uSW5pdCgpOmFueSB7XG4gICAgdGhpcy5nZXRDb250YWN0cygpO1xuICB9XG59XG4iXX0=
