import {Contact} from "./contact";
export const CONTACTS: Contact[] = [
  {firstname: "Max", lastname : "Maxyee", phone: "01680057586" ,
  email: "Maxyee@gmail.com"},
  {firstname: "Eyamin", lastname : "Khan", phone: "01675556582" ,
  email: "eyamin@gmail.com"},
  {firstname: "Mohon", lastname : "Khan", phone: "01521213203" ,
  email: "mohon@gmail.com"},
  {firstname: "Julhas", lastname : "hossain", phone: "01780015384" ,
  email: "julhas@gmail.com"},
];
