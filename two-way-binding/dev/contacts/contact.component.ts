import {Component} from "angular2/core";

@Component({
    selector: 'contact',
    template:`
    <input [(ngModel)]="contact.firstname" type="text"/>

    <div>
        <div>
            <label for="firstname"> First Name: </label>
            <input [(ngModel)]="contact.firstname" type="text" id="firstname"/>
        </div>
        <div>
            <label for="lastname"> Last Name: </label>
            <input [(ngModel)]="contact.lastname" type="text" id="lastname"/>
        </div>
        <div>
            <label for="phone"> Phone Number: </label>
            <input [(ngModel)]="contact.phone" type="text" id="phone"/>
        </div>
        <div>
            <label for="email"> Email: </label>
            <input [(ngModel)]="contact.email" type="text" id="email"/>
        </div>
    </div>
    `,
    inputs: ["contact"],
    styles: [`
        label{
          display:inline-block;
          widht:140px;
        }

        input {
          width:250px;
        }
    `]
})

export class ContactComponent {
    public contact = {};
}
