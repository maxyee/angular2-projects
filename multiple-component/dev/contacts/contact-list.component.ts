import {Component} from "angular2/core";
import {ContactComponent}  from "./contact.component";

@Component({
    selector: 'contact-list',
    template:`
    <ul>
        <li *ngFor="#contact of contacts"
        (click)="onSelect(contact)"
        [class.clicked]="SelectedContact === contact"
        >
        {{ contact.firstname }} {{ contact.lastname }}
        </li>
    </ul>
    <contact [contact]="SelectedContact"></contact>
    `,
    directives: [ContactComponent],
    styleUrls: ["../src/css/app.css"]
})

export class ContactListComponent {
  public contacts = [
    {firstname: "Max", lastname : "Maxyee", phone: "01680057586" ,
    email: "Maxyee@gmail.com"},
    {firstname: "Eyamin", lastname : "Khan", phone: "01675556582" ,
    email: "eyamin@gmail.com"},
    {firstname: "Mohon", lastname : "Khan", phone: "01521213203" ,
    email: "mohon@gmail.com"},
    {firstname: "Julhas", lastname : "hossain", phone: "01780015384" ,
    email: "julhas@gmail.com"},
    ];

  public SelectedContact = {};

  onSelect(contact){
    this.SelectedContact = contact;
  }
}
