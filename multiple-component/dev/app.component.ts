import {Component} from 'angular2/core';
import {ContactListComponent}  from "./contacts/contact-list.component";

@Component({
    selector: 'my-app',
    template: `
        <contact-list></contact-list>

        <!--
        <h3 (click)="onSelect()"
        [class.clicked]="showDetail === true"
        >{{ contact.firstname }} {{ contact.lastname }}
        </h3>
        -->

        <!--
        <input [(ngModel)]="SelectedContact.firstname" type="text"/>

        <div>
            phone Number:{{ SelectedContact.phone }}<br>
            Email :{{SelectedContact.email }}
        </div>
        -->
    `,
    directives: [ContactListComponent],
    
})
export class AppComponent {

}
