import {Component} from "angular2/core";
import {ContactComponent}  from "./contact.component";
import {ContactService} from "./contact.service";
import {Contact} from "./contact";
import {OnInit} from "angular2/core";

@Component({
    selector: 'contact-list',
    template:`
    <ul>
        <li *ngFor="#contact of contacts"
        (click)="onSelect(contact)"
        [class.clicked]="SelectedContact === contact"
        >
        {{ contact.firstname }} {{ contact.lastname }}
        </li>
    </ul>
    <contact [contact]="SelectedContact"></contact>
    `,
    directives: [ContactComponent],
    providers: [ContactService],
    styleUrls: ["../src/css/app.css"]
})

export class ContactListComponent implements OnInit {
  public contacts = Contact[];

  public SelectedContact = {};

  constructor(private _contactService: ContactService) {}

  onSelect(contact){
    this.SelectedContact = contact;
  }

  getContacts() {
    this._contactService.getContacts().then((contacts: Contact[]) => this.contacts = contacts);
  }

  ngOnInit():any {
    this.getContacts();
  }
}
